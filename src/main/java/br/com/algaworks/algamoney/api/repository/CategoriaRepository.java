package br.com.algaworks.algamoney.api.repository;

import br.com.algaworks.algamoney.api.model.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author @gleysongama
 */
public interface CategoriaRepository extends JpaRepository<Categoria, Long> {
  
}

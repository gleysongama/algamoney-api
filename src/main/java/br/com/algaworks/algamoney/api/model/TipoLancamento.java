package br.com.algaworks.algamoney.api.model;

public enum TipoLancamento {

	RECEITA("Receita"),
    DESPESA("Despesa"),;
    
    private String tipoLancamento;

    TipoLancamento(String tipoLancamento ) {
        this.tipoLancamento = tipoLancamento;
    }
    
    public String getTipoLancamento() {
        return this.tipoLancamento;
    }
}

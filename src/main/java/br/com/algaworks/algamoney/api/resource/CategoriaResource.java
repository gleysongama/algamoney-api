package br.com.algaworks.algamoney.api.resource;

import br.com.algaworks.algamoney.api.model.Categoria;
import br.com.algaworks.algamoney.api.repository.CategoriaRepository;
import java.net.URI;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author @gleysongama
 */
@RestController
@RequestMapping("/categorias")
public class CategoriaResource {

	@Autowired
	CategoriaRepository categoriaRepository;

	/**
	 * Metodo que retona a lista de categorias
	 * 
	 * @return Lista com as categorias cadastradas no banco de dados
	 * @author @gleysongama
	 */
	@GetMapping
	public ResponseEntity<?> listar() {
		List<Categoria> categorias = categoriaRepository.findAll();
		return !categorias.isEmpty() ? ResponseEntity.ok(categorias) : ResponseEntity.noContent().build();
	}

	/**
	 * Metodo que cadastra uma nova categoria
	 * 
	 * @return O objeto que acabou de ser persistido no banco de dados
	 * @param response HttpServletResponse
	 * @param categoria Categoria - Objeto JSON que será convertido em uma entidade modelo
	 * @author @gleysongama
	 */
	@PostMapping
	// @ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> criar(@Valid @RequestBody Categoria categoria, HttpServletResponse response) {
		Categoria categoriaSalva = categoriaRepository.save(categoria);

		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri()
											 .path("/{codigo}")
											 .buildAndExpand(categoriaSalva.getCodigo())
											 .toUri();
		response.setHeader("Location", uri.toASCIIString());

		return ResponseEntity.created(uri).body(categoriaSalva);
	}

	/**
	 * Metodo que retorna uma categoria cadastrada no banco de dados a partir do
	 * parâmetro codigo
	 * 
	 * @param codigo Long - parâmetro com o código da entidade no banco de dados
	 * @return O objeto persistido no banco de dados
	 * @author @gleysongama
	 */
	@GetMapping("/{codigo}")
	public ResponseEntity<?> buscaPeloCodigo(@PathVariable Long codigo) {
		Categoria categoria = categoriaRepository.findOne(codigo); // com spring-boot 1.5.15.RELEASE
		return categoria != null ? ResponseEntity.ok(categoria) : ResponseEntity.notFound().build();
	}
}
